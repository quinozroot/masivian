package masivianentrega;

import masivianentrega.controller.PruebaController;

public class Main {

    public static void main(String[] args) {

       //Instancea la clase controlladora 
       PruebaController prueba = new PruebaController();
       
       //Ejecuta el metodo del controlador que tiene la logica de los pasos a ejecutar
       prueba.coreografo();
       
        
    }

}
