/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masivianentrega.controller;

import masivianentrega.dtos.Dato;
import masivianentrega.utils.Mensaje;

/**
 *
 * @author Jorge Linares
 */
public class PruebaController {
    
    //Obtiene el singleton de la clase Dato
    private final Dato data = Dato.getInstance();
    
    //Genera una nueva instance de la clase que controla los mensajes
    private final Mensaje mensaje = new Mensaje();
    
    /**
     * Metodo que tiene las instrucciones a procesar
     */
    public void coreografo(){
        
        //Realiza el acomodo de las varibles
        data.control();
        
        //Inicia el recorrido de numeros primos
        while(data.pageOffSet <= Dato.M){
             
             //Imprime la cabecera por cada mil
             this.mensaje.mensageCabecera();
             
            //For que navega entre las paginaciones de los datos
            for (data.rowOffSet=data.pageOffSet; data.rowOffSet <= data.pageOffSet+data.RR-1; data.rowOffSet++) {
                
                //Ordena e imprime cada numero primo
                this.mensaje.mensajeEjecucion();
                System.out.println();
            }
            System.out.println("\f");
            
            //Genera un aumento en la paginacion
            this.data.aumento();
        }
    }
    
    
    
}
