/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masivianentrega.dtos;

/**
 *
 * @author Jorge Linares
 */

/**
 * 
 * Clase que genera los datos que se van a trabajae
 */
public class Dato {
    
    //Crea variable para controlar una unica instancia de la clase
    public static Dato instance;
    
    /**
     * Metodo que permite el Singleton de la clase
     * @return Retorna la instancia de la clase 
     */
    public static Dato getInstance(){
        //
        if(instance == null){
           instance = new Dato(); 
        }
        return instance;
    }
    
    //Se declaran constantes que se van a trabajar
    public static final int M = 1000;
    public static final int ORDMAX = 30;
    private final int MULT[] = new int[ORDMAX + 1];
    public  final int RR = 50;
    public  final int CC = 4;
        
    //Se declaran las variables publicas que se van a trabajar
    public int pageNumber;
    public int pageOffSet;
    public int rowOffSet;
    public int p[] = new int[M + 1];
    
    //Se declaran las variables privadas de la clase
    private int j;    
    private int k;
    private boolean jPrime;
    private int ord;
    private int square;
    private int n;
   
    //Inicializamos las variables
    public Dato() {
        
        this.j = 1;
        this.k = 1;
        this.p[1] = 2;
        this.ord = 2;
        this.square = 9;
        this.pageNumber = 1;
        this.pageOffSet = 1;
        this.n = 0;

    }
    
    //Metodo que acomoda
    public void control(){
        
        while(k<M){
            do{
                j+=2;
                if( j == square){
                    ord++;
                    square = p[ord]*p[ord];
                    MULT[ord-1] = j;
                }
                n=2;
                jPrime=true;
                while(n<ord && jPrime){
                    while(MULT[n]<j)
                        MULT[n] += p[n] + p[n];
                    
                    if(MULT[n] == j)
                        jPrime = false;
                    n++;
                }
            }while(!jPrime); 
            
            k++;
            p[k] = j;
        }
    }
    
    /**
     * Metodo que aumenta la paginacion de la variable para obtener mas datos
     */
    public void aumento(){
        pageNumber++;
        pageOffSet += RR * CC;
    }
    
}
