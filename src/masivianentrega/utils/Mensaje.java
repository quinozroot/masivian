/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package masivianentrega.utils;

import masivianentrega.dtos.Dato;

/**
 *
 * @author Jorge Linares
 */
public class Mensaje {
    
    //Obtiene la instancia de la clase
    private final Dato data = Dato.getInstance();
    
    
    /**
     * Metodo que imprime los datos que se encuetran en memoria
     * para mostrar cada mil datos
     */
    public void mensageCabecera(){
        
        System.out.println("The First");
        System.out.println(Integer.toString(Dato.M));
        System.out.println("Prime Numbers === Page");
        System.out.println(Integer.toString(data.pageNumber));
        System.out.println("\n");
            
            
    }
    
    /**
     * Clase que imprime los numeros primos segun la cantidad acordada
     */
    public void mensajeEjecucion(){
        for(int _C = 0; _C <= data.CC - 1; _C++){
            if(data.rowOffSet + _C * data.RR <=Dato.M ){
                System.out.printf("%10d",data.p[data.rowOffSet + _C * data.RR]);
            }
        }
    }
    
}
